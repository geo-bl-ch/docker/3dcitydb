FROM registry.gitlab.com/geo-bl-ch/docker/postgis:3.0-postgresql12

LABEL maintainer="Karsten Deininger <karsten.deininger@bl.ch>"

# Switch to root user for installing dependencies
USER 0

# Copy 3DCityDB scripts and SQLs
COPY 3dcitydb/PostgreSQL/ /opt/3dcitydb/

# Copy initialization script for 3DCityDB
COPY --chown=26:0 postgresql-start/* /opt/app-root/src/postgresql-start/

ENV EPSG="2056" \
    HEIGHT_EPSG="0" \
    SKIP_INIT=false

USER 26
