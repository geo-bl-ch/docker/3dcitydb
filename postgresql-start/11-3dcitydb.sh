#!/bin/bash

set -e

if [ $SKIP_INIT != "true" ]; then

  echo "running 3dcitydb initialization"

  if [ $HEIGHT_EPSG -gt 0 ]; then
    GMLSRSNAME=urn:ogc:def:crs,crs:EPSG::$EPSG,crs:EPSG::$HEIGHT_EPSG
  else
    GMLSRSNAME=urn:ogc:def:crs:EPSG::$EPSG
  fi

  cd /opt/3dcitydb/SQLScripts
  psql -U "$POSTGRESQL_USER" -d "$POSTGRESQL_DATABASE" -f "/opt/3dcitydb/SQLScripts/CREATE_DB.sql" -v srsno="$EPSG" -v gmlsrsname="$GMLSRSNAME"

else

  echo "skipping 3dcitydb initialization"

fi